<?php

declare(strict_types=1);

class ArrayFlatten
{
    /**
     * Delimiter level.
     *
     * @var string
     */
    const DELIMITER_LEVEL = '.';
    /**
     *Delimiter level numeric index.
     *
     * @var string
     */
    const DELIMITER_ARRAY = '[%s]';

    /**
     * @var array
     */
    protected $originArray = [];
    /**
     * @var array
     */
    protected $flattenArray = [];

    /**
     * ArrayFlatten constructor.
     *
     * @param array $originArray
     */
    public function __construct(array $originArray = [])
    {
        $this->originArray = $originArray;
    }

    /**
     * Get origin data.
     *
     * @return array
     */
    public function getOriginArray(): array
    {
        return $this->originArray;
    }

    /**
     * Set origin data.
     *
     * @param array $originArray
     */
    public function setOriginArray(array $originArray)
    {
        $this->originArray = $originArray;
    }

    /**
     * Get flatten data.
     *
     * @return array
     */
    public function getFlattenArray(): array
    {
        return $this->flattenArray;
    }

    /**
     * Processing flatten.
     *
     * @param string     $levelIndexName
     * @param array|null $data
     */
    public function flattArray(string $levelIndexName = '', array $data = null)
    {
        if ($data === null) {
            $data = $this->getOriginArray();
        }

        foreach ($data as $key => $value) {
            if (is_array($value)) {
                $this->flattArray($key, $value);
            } else {
                $this->flattenArray[$this->prepareKeyName($levelIndexName, $key)] = $value;
            }
        }
    }

    /**
     * Prepare key name.
     *
     * @param string $level
     * @param string $name
     *
     * @return string
     */
    protected function prepareKeyName(string $level, string $name): string
    {
        if (is_numeric($name)) {
            $name = $level.$this->prepareArrayKeyName($name);
        } else {
            $name = implode(self::DELIMITER_LEVEL,
                [
                    $level,
                    $name,
                ]);
        }

        return ltrim($name, self::DELIMITER_LEVEL);
    }

    /**
     * Prepare array key name.
     *
     * @param int $index
     *
     * @return string
     */
    protected function prepareArrayKeyName(int $index)
    {
        return sprintf(self::DELIMITER_ARRAY, $index);
    }
}